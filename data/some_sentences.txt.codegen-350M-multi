Prompt:
 ;;; Translate sentences into Lisp expressions

;sentence: southern France
AMR: (s / south :part-of (c / country :wiki "France" :name (n / name :op1 "France")))

;sentence: She talked to him in French.
AMR: (t / talk-01 :ARG0 (s / she) :ARG2 (h / he) :medium (l / language :wiki "French_language" :name (n / name :op1 "French")))

;sentence: the south of France
AMR: (s / south :part-of (c / country :wiki "France" :name (n / name :op1 "France")))

;sentence: He is my brother-in-law.
AMR: (h / have-rel-role-91 :ARG0 (h2 / he) :ARG1 (i / i) :ARG2 (b / brother-in-law))

;sentence: I hardly know her.
AMR: (k / know-01 :ARG0 (i / i) :ARG1 (s / she) :degree (h / hardly))

;sentence: My friend lives in Italy
Expected:
# ::snt My friend lives in Italy
(l / live-01
      :ARG0 (p / person
            :ARG0-of (h / have-rel-role-91
                  :ARG1 (ii / i)
                  :ARG2 (f / friend)))
      :location (c / country
            :name (n / name
                  :op1 "Italy")))
Generated:
.
 (k / have-rel-role-91 :ARG0 (k2 / he) :ARG1 (i / i) :ARG2 (b / brother-in-law))


Precision: 0.625, Recall: 0.3333333333333333, F-score: 0.43478260869565216

##############

Prompt:
 ;;; Translate sentences into Lisp expressions

;sentence: Whose toy did the girl find?
AMR: (f / find-01 :ARG0 (g / girl) :ARG1 (t / toy :poss (a / amr-unknown)))

;sentence: How did the girl find the boy?
AMR: (f / find-01 :ARG0 (g / girl) :ARG1 (b / boy) :manner (a / amr-unknown))

;sentence: The machine was adjusted by the girl.
AMR: (a / adjust-01 :ARG0 (b / girl) :ARG1 (m / machine))

;sentence: There was shouting, and the boy left.
AMR: (a / and :op1 (c / shout-01) :op2 (l / leave-11 :ARG0 (b / boy)))

;sentence: Where did the girl find the boy?
AMR: (f / find-01 :ARG0 (g / girl) :ARG1 (b / boy) :location (a / amr-unknown))

;sentence: The robot gave a talk on the main stage
Expected:
# ::snt The robot gave a talk on the main stage
(t / talk-01
      :ARG0 (r / robot)
      :location (s / stage
            :mod (m / main)))
Generated:
.
 (a / talk-01 :ARG0 (b / girl) :ARG1 (m / machine))


Precision: 0.5, Recall: 0.375, F-score: 0.42857142857142855

##############

Prompt:
 ;;; Translate sentences into Lisp expressions

;sentence: the man attracts
AMR: (a / attract-01 :ARG0 (m / man))

;sentence: the man is attractive
AMR: (a / attract-01 :ARG0 (m / man))

;sentence: He is not tall enough to ride the rollercoaster.
AMR: (h / have-degree-91 :polarity - :ARG1 (h2 / he) :ARG2 (t / tall) :ARG3 (e / enough) :ARG6 (r / ride-01 :ARG0 h2 :ARG1 (r2 / rollercoaster)))

;sentence: The boy wants to go.
AMR: (w / want-01 :ARG0 (b / boy) :ARG1 (g / go-02 :ARG0 b))

;sentence: The boy prefers to go.
AMR: (p / prefer-01 :ARG0 (b / boy) :ARG1 (g / go-02 :ARG0 b))

;sentence: The tall man wants to go
Expected:
# ::snt The tall man wants to go
(w / want-01
      :ARG0 (m / man
            :mod (t / tall))
      :ARG1 (g / go-02
            :ARG0 m))
Generated:
.
 (t / want-01 :ARG0 (b / boy) :ARG1 (g / go-02 :ARG0 b))


Precision: 0.8571428571428571, Recall: 0.6666666666666666, F-score: 0.75

##############

Prompt:
 ;;; Translate sentences into Lisp expressions

;sentence: the car that is not black
AMR: (c / car :ARG1-of (b / black-04 :polarity -))

;sentence: his car
AMR: (c / car :poss (h / he))

;sentence: the black car
AMR: (c / car :ARG1-of (b / black-04))

;sentence: the car of his
AMR: (c / car :poss (h / he))

;sentence: I have no money.
AMR: (h / have-03 :polarity - :ARG0 (i / i) :ARG1 (m / money))

;sentence: She doesn't need a car
Expected:
# ::snt She doesn't need a car
(n / need-01
      :polarity -
      :ARG0 (s / she)
      :ARG1 (c / car))
Generated:
.
 (h / have-03 :polarity - :ARG0 (i / i) :ARG1 (m / money))


Precision: 0.5714285714285714, Recall: 0.5714285714285714, F-score: 0.5714285714285714

##############

Prompt:
 ;;; Translate sentences into Lisp expressions

;sentence: I drove through the tunnel.
AMR: (d / drive-01 :ARG0 (i / i) :path (t / tunnel))

;sentence: How fast did the girl run?
AMR: (r / run-01 :ARG0 (g / girl) :manner (f / fast :degree (a / amr-unknown)))

;sentence: He drove west.
AMR: (d / drive-01 :ARG0 (h / he) :direction (w / west))

;sentence: The road goes on forever.
AMR: (g / go-on-15 :ARG1 (r / road) :extent (f / forever))

;sentence: What purple thing did the girl see?
AMR: (s / see-01 :ARG0 (g / girl) :ARG1 (a / amr-unknown :ARG1-of (p / purple-02)))

;sentence: how did the chicken cross the road?
Expected:
# ::snt how did the chicken cross the road?
(c / cross-02
      :ARG0 (c2 / chicken)
      :ARG1 (r / road)
      :manner (a / amr-unknown))
Generated:

 (c / cross-01 :ARG0 (g / chicken) :ARG1 (a / amr-unknown :ARG1-of (p / purple-02)))


Precision: 0.5, Recall: 0.5, F-score: 0.5

##############

Average:
Precision: 0.6107142857142858, Recall: 0.4892857142857142, F-score: 0.5369565217391304