## Few-shot AMR-LLM-PARSER
An [Abstract Meaning Representation (AMR)](https://amr.isi.edu/) parser for natural language text that 
leverages Large Language Models (LLMs).

This is an experiment to evaluate code-oriented LLMs capabilities to learn to parse English text to AMR, following
the idea by [Shin and Van Durme](https://arxiv.org/abs/2112.08696).

Currently, you can choose either [OpenAI codex](https://openai.com/blog/openai-codex/)
or [Salesforce CodeGen](https://github.com/salesforce/codegen)

### Usage

- Install requirements in [requirements.txt](requirements.txt) with
```
pip install -r requirements.txt
```
- If you want to evaluate Codex, you need to get an [OpenAI API key](https://openai.com/api/), and declare it in your environment as
```
export OPENAI_API_KEY=<your key>
```
- Run a default version of [src/llm_parser.py](src/llm_parser.py) as:
```
python src/llm_parser.py -p <reference_parses>
```
You need to provide at least the `<reference_parses>` file, with a structure like
that in [amr_tutorial.txt](data/amr_tutorial.txt).

Optionally, you can pass a file with the `-s <sentences>` argument, to evaluate the sentences in it. This file
requires sentences, one per line (cf. [some_sentences.txt](data/some_sentences.txt)).

If only `<reference_parses>` is given, the code evaluates the
LLM performance using eval data taken from a split of this file.

Some example evaluations are found in the [data](data) folder.

The rest of the parameters for [llm_parser.py](src/llm_parser.py) are:
```
usage: llm_parser.py [-h] -p PARSES [-s SENTENCES] [-t TOP]
                     [-m {codex,codegen}] [-z {350M,2B,6B,16B}]
                     [-l {mono,multi}] [-v]

options:
  -h, --help            show this help message and exit
  -p PARSES, --parses PARSES
                        Name of the file with reference parses
  -s SENTENCES, --sentences SENTENCES
                        Name of file with sentences to parse
  -t TOP, --top TOP     Build a parse with top parses
  -m {codex,codegen}, --model_fam {codex,codegen}
                        Name of language model family to use
  -z {350M,2B,6B,16B}, --model_size {350M,2B,6B,16B}
                        Only for codegen, size of pretrained model to use
  -l {mono,multi}, --model_lang {mono,multi}
                        Use multi language model, or fine-tuned in python
                        (mono)
  -v, --verbose
```

### To do
Implement a function that tells when a partial AMR is gramatically correct, to guide
the LLM generation, as per [Shin et al.](https://aclanthology.org/2021.emnlp-main.608/)
