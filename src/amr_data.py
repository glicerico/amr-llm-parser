from sklearn.model_selection import train_test_split


class AMRData:
    """
    Reads (sentence, graph) pairs in the format:
    ; sentence: XXXX
    AMR: YYYYY
    """
    def __init__(self, data_file):
        self.data_file = data_file
        self.samples = []

    def parse_data(self):
        line_index = 0
        with open(self.data_file) as f:
            lines = f.readlines()
            while line_index < len(lines):
                if lines[line_index].startswith("sentence:"):
                    # Make sure only the first colon is removed, in case sentence contains them
                    sentence = ":".join(lines[line_index].split(":")[1:]).strip()
                    graph = ":".join(lines[line_index + 1].split(":")[1:]).strip()
                    line_index += 2
                    self.samples.append([sentence, graph])
                else:
                    line_index += 1  # Skip lines not starting with ";sentence"
        return self.samples

    def split_data(self, test_size=0.2):
        """
        Split the samples into prompt and evaluation sets
        :param test_size: float, the proportion of data to use as the evaluation set (default: 0.2)
        :return: tuple of (prompt_data, evaluation_data)
        """
        self.parse_data()
        X = [d[0] for d in self.samples]
        y = [d[1] for d in self.samples]

        X_train, X_val, y_train, y_val = train_test_split(X, y, test_size=test_size)
        prompt_data = list(zip(X_train, y_train))
        evaluation_data = list(zip(X_val, y_val))

        return prompt_data, evaluation_data

