from sentence_transformers import SentenceTransformer, util


class Similarities:
    """
    Calculates and sorts similarities between lists of sentences
    """

    def __init__(self, prompt_sents, chkpt='sentence-transformers/all-MiniLM-L6-v2'):
        """
        :param chkpt: model checkpoint
        :param prompt_sents: list of sentences to calculate similarities with
        """
        self.model = SentenceTransformer(chkpt)
        self.prompt_sents = prompt_sents

    def calculate(self, sentence):
        """
        Calculate and sort sentence similarities
        :param sentence: sentence to compare against prompt sentences
        :return: Sorted list of similarities and the id of corresponding sentence
        """
        prompt_embeddings = self.model.encode(self.prompt_sents)
        sentence_embedding = self.model.encode(sentence)
        raw_sims = util.cos_sim(prompt_embeddings, sentence_embedding)
        similarities = []
        for idx, sim in enumerate(raw_sims):
            similarities.append((sim, idx))

        sorted_sims = sorted(similarities, key=lambda x: x[0], reverse=True)
        return sorted_sims
