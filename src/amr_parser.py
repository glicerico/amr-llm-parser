import amrlib
import spacy


class AMRParser:
    def __init__(self, sentences_file):
        amrlib.setup_spacy_extension()
        self.nlp = spacy.load('en_core_web_sm')
        self.sentences_file = sentences_file

    def parse(self):
        """
        Parse each sentence in sentences_file
        """
        samples = []
        with open(self.sentences_file, 'r') as f:
            lines = f.readlines()
            lines = [line.strip() for line in lines if line.strip()]
            for line in lines:
                parse = self.stog(line)
                samples.append([line, parse])
        return samples

    def stog(self, sentence):
        doc = self.nlp(sentence)
        graphs = doc._.to_amr()
        return graphs[0]  # Only return first graph



