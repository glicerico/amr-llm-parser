from io import StringIO

from smatch import score_amr_pairs

from src import AMRData, PromptCreator, Generator, AMRParser


def evaluate_parses(graph1, graph2):
    """
    Evaluate two parses using smatch
    """
    try:
        file1 = StringIO(graph1)
        file2 = StringIO(graph2)
        return next(score_amr_pairs(file1, file2))
    except:
        print(f"Exception caught when scoring AMR {graph1} with its reference pair, probably due to malformation.")
        return (0, 0, 0)


def generate_parse(prompter, generator, sentence, verbose=False):
    """
    Generate a prompt as context for the given sentence.
    Then generate a parse.
    """
    prompt = prompter.create_prompt(sentence)
    response = generator.generate(prompt, sentence)
    parse = None
    if generator.model_fam == 'codex':
        parse = response['choices'][0].text  # Take first generation
    elif generator.model_fam == 'codegen':
        parses = response.split(sentence)[-1]  # Keep generation only
        parse = parses.split(';')[0]  # Take only first generation
    prefix = "AMR:"
    if prefix in parse:
        parse = parse.replace(prefix, "")
    if verbose:
        print(f"###########\nPrompt:\n{prompt}\nParse:{parse}")
    return prompt, parse


def main(arguments):
    """
    Run AMR parser with given arguments
    """
    # Get args
    parses_file = arguments.parses
    top = arguments.top
    model_engine = arguments.model_size + "-" + arguments.model_lang
    model_fam = arguments.model_fam
    response_appendix = "." + model_fam
    if model_fam == 'codegen':
        response_appendix += "-" + model_engine
    response_filename = parses_file + response_appendix
    sentences_file = arguments.sentences

    # Get prompt and eval data
    amr_data = AMRData(parses_file)
    if sentences_file is not None:
        # Read data and generate parses
        prompt_data = amr_data.parse_data()
        eval_data = AMRParser(sentences_file).parse()
        response_filename = sentences_file + response_appendix
    else:
        # Read data from file
        prompt_data, eval_data = amr_data.split_data(test_size=0.1)

    # Display info
    if model_fam == "codegen":
        print(f"Running {model_fam}, {model_engine}, with {top} pairs in prompt.")
    else:
        print(f"Running {model_fam}, with {top} pairs in prompt.")
    print(f"Prompts file: {parses_file}, sentences file: {sentences_file}")

    # Prepare objects
    prompter = PromptCreator(prompt_data, top=top)
    generator = Generator(model_fam=model_fam,
                          model_engine=model_engine)
    prec_sum = 0
    recall_sum = 0
    f_score_sum = 0
    len_data = 0
    with open(response_filename, 'w') as fo:
        # Generate and evaluate parse for each test sentence
        for sent, reference in eval_data:
            len_data += 1
            prompt, res = generate_parse(prompter, generator, sent, verbose=True)
            precision, recall, f_score = evaluate_parses(res, reference)
            prec_sum += precision
            recall_sum += recall
            f_score_sum += f_score
            fo.write(f"Prompt:\n {prompt}\n")
            fo.write(f"Expected:\n{reference}\n")
            fo.write(f"Generated:\n{res}\n")
            fo.write(f"Precision: {precision}, Recall: {recall}, F-score: {f_score}\n\n##############\n\n")

        fo.write("Average:\n")
        fo.write(f"Precision: {prec_sum / len_data}, Recall: {recall_sum / len_data}, F-score: {f_score_sum / len_data}")


if __name__ == "__main__":
    import argparse

    # Parse CLI args
    parser = argparse.ArgumentParser()
    parser.add_argument('-p', '--parses',
                        type=str,
                        required=True,
                        help='Name of the file with reference parses')
    parser.add_argument('-s', '--sentences',
                        type=str,
                        help='Name of file with sentences to parse')
    parser.add_argument('-t', '--top',
                        type=int,
                        default=5,
                        help='Build a parse with top reference parses')
    parser.add_argument('-m', '--model_fam',
                        type=str,
                        choices=['codex', 'codegen'],
                        default='codex',
                        help='Name of language model family to use')
    parser.add_argument('-z', '--model_size',
                        type=str,
                        choices=['350M', '2B', '6B', '16B'],
                        default='350M',
                        help='Only for codegen, size of pretrained model to use')
    parser.add_argument('-l', '--model_lang',
                        type=str,
                        choices=['mono', 'multi'],
                        default='multi',
                        help='Use multi language model, or fine-tuned in python (mono)')
    parser.add_argument('-v', '--verbose',
                        action='store_true')
    args = parser.parse_args()

    main(args)
