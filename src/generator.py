import os

import openai


class Generator:
    def __init__(self, model_fam='codex', model_engine='350M-multi'):
        # Set the API key
        self.model_fam = model_fam
        self.model_engine = model_engine
        self.tokenizer = None
        self.device = None
        self.model = None
        if self.model_fam == "codex":
            self.setup_codex()
        elif self.model_fam == "codegen":
            self.setup_codegen()

    def setup_codegen(self):
        # Set the Codegen tokenizer and model
        import torch
        from transformers import AutoTokenizer, AutoModelForCausalLM
        self.model_engine = "Salesforce/codegen-" + self.model_engine
        self.tokenizer = AutoTokenizer.from_pretrained(self.model_engine)
        self.device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
        if torch.cuda.is_available():
            torch.set_default_tensor_type(torch.cuda.FloatTensor)
        else:
            torch.set_default_tensor_type(torch.FloatTensor)
        self.model = AutoModelForCausalLM.from_pretrained(self.model_engine).to(self.device)

    def setup_codex(self):
        # Set the CODEX API and engine
        openai.api_key = os.environ["OPENAI_API_KEY"]
        self.model_engine = "code-davinci-002"

    def generate(self, prompt, sentence):
        completion = ''
        if self.model_fam == 'codex':
            # Call the Codex API
            completion = openai.Completion.create(engine=self.model_engine, prompt=prompt, max_tokens=1000,
                                                  temperature=0.01, stop=";")
        elif self.model_fam == 'codegen':
            inputs = self.tokenizer(prompt, return_tensors="pt").to(self.device)
            sample = self.model.generate(**inputs, max_length=1024, pad_token_id=self.tokenizer.eos_token_id)
            completion = self.tokenizer.decode(sample[0], truncate_before_pattern=[r"\n\n^#", "^'''", "\n\n\n"])

        return completion
