import unittest
from src import PromptCreator


class TestPromptCreator(unittest.TestCase):
    def setUp(self):
        prompt_data = [("The boy is playing outside", "AMR for sentence 1"),
                       ("The boy is young", "AMR for sentence 2"),
                       ("She wrote three new books this year", "AMR for sentence 3")]
        self.prompt_creator = PromptCreator(prompt_data, top=3)

    def test_create_prompt(self):
        sentence = "Bell Hooks is a renown author"
        prompt = self.prompt_creator.create_prompt(sentence)
        self.assertIn("The boy is young", prompt)
        self.assertIn("The boy is playing outside", prompt)
        self.assertIn("She wrote three new books this year", prompt)
        self.assertIn("AMR for sentence 1", prompt)
        self.assertIn("AMR for sentence 2", prompt)
        self.assertIn("AMR for sentence 3", prompt)
        self.assertIn(";sentence: " + sentence, prompt)
        print(prompt)


if __name__ == '__main__':
    unittest.main()
