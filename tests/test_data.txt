;sentence: The girl adjusted the machine.
AMR: (a / adjust-01 :ARG0 (b / girl) :ARG1 (m / machine))

;sentence: The machine was adjusted by the girl.
AMR: (a / adjust-01 :ARG0 (b / girl) :ARG1 (m / machine))

;sentence: The killing happened yesterday.
AMR: (k / kill-01 :time (y / yesterday))

;sentence: The killing took place yesterday.
AMR: (k / kill-01 :time (y / yesterday))

;sentence: The boy doesn’t know the girl came.
AMR: (k / know-01 :polarity - :ARG0 (b / boy) :ARG1 (c / come-01 :ARG1 (g / girl)))

;sentence: Do you want tea or coffee?
AMR: (w / want-01 :ARG0 (y / you) :ARG1 (a / amr-choice :op1 (t / tea) :op2 (c / coffee)))
